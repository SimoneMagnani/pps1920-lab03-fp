package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.Modules.Person
import u03.Lists
import u03.Lists.List._
import u03.Streams._

class TestList {

  val lst: Cons[Int] = Cons(10, Cons(20, Cons(30, Nil())))
  val personLst:Lists.List[Person] = Cons(Person.Teacher("pluto","pps"), Cons(Person.Student("pippo",12), Cons(Person.Teacher("paperino","pcd"), Nil())))
  val lstFold = Cons(3, Cons(7, Cons(1, Cons(5, Nil ()))))
  val s = Stream.take(Stream.iterate(0)( _ +1))(10)

  @Test def testDrop(){
    assertEquals(Cons (20 , Cons (30 , Nil ())),drop(lst, 1)) // Cons (20 , Cons (30 , Nil ()))
    assertEquals(Cons (30 , Nil ()), drop(lst, 2)) // Cons (30 , Nil ())
    assertEquals(Nil (), drop(lst, 5))// Nil ()
  }

  @Test def testFlatMap() {
    assertEquals(Cons(11, Cons(21, Cons(31, Nil()))),
        flatMap (lst)(v => Cons(v+1, Nil())))
    assertEquals(Cons (11, Cons (12, Cons (21, Cons (22, Cons (31, Cons (32, Nil())))))),
        flatMap ( lst )(v=> Cons(v+1, Cons(v+2, Nil()))))
  }

  @Test def testMapFlatMap() {
    assertEquals(Cons(20, Cons(40, Cons(60, Nil()))), mapFlatMap(lst)(e => e * 2))
    assertEquals(map(Nil())(_=>2), mapFlatMap(Nil())(_ => 2))
  }

  @Test def testFilterFlatMap() {
    assertEquals(Cons(30, Nil()), filter(lst)(e => e > 25))
  }

  @Test def testMax() {
    assertEquals(max(Cons(10, Cons(25, Cons(20, Nil())))), Optionals.Option.Some(25))
    assertEquals(max(Nil()), Optionals.Option.None())
  }

  @Test def testGetCourses(){
    assertEquals(Cons("pps", Cons("pcd", Nil())),Person.getCourses(personLst))
  }

  @Test def testGetCoursesFlatMap(){
    assertEquals(Cons("pps", Cons("pcd", Nil())),Person.getCoursesFlatMap(personLst))
  }

  @Test def testFoldLeft(){
    assertEquals(-16,foldLeft( lstFold )(0)(_ - _ )) // -16
  }

  @Test def testReverse(){
    assertEquals(Cons(30, Cons(20, Cons(10, Nil()))),reverse( lst ))
  }

  @Test def testFoldRight(){
    assertEquals(-8,foldRight( lstFold )(0)(_ - _ )) // -8
    assertEquals(-7.5,foldRightUsingTailRec( lstFold )(0.5)(_ - _ )) // -8
  }

  @Test def testStreamDrop(){
    assertEquals(Cons(6, Cons(7, Cons(8, Cons(9, Nil ())))),Stream.toList(Stream.drop(s)(6)))// = > Cons (6 , Cons (7 , Cons (8 , Cons (9 , Nil ()))))
    assertEquals(Stream.empty(),Stream.drop(Stream.empty())(6))
  }

  @Test def testStreamConstant() {
    assertEquals(Cons("x", Cons("x", Cons("x", Cons("x", Nil ())))),Stream.toList(Stream.take(Stream.constant("x"))(4)))
    assertEquals(Cons(2, Cons(2, Cons(2, Cons(2, Nil ())))),Stream.toList(Stream.take(Stream.constant(2))(4)))
  }


  @Test def testStreamFibs() {
    assertEquals(Cons(0, Cons(1, Cons(1, Cons(2, Cons(3, Cons(5, Cons(8, Cons(13, Nil())))))))), Stream.toList(Stream.take(Stream.fibs())(8)))
  }

}