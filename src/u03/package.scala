package u03

import u03.Lists.List._


object es1 extends App {
  /*Consider the List type introduced in class. Analogously to sum and
    append, create the following functions:
    a) def drop[A](l: List[A], n: Int): List[A]*/
  override def main(args: Array[String]): Unit = {

    val lst = Cons(10, Cons(20, Cons(30, Nil())))
    println(drop(lst, 1)) // Cons (20 , Cons (30 , Nil ()))
    println(drop(lst, 2)) // Cons (30 , Nil ())
    println(drop(lst, 5))// Nil ()
  }


}
