package u03
import u02.Optionals._
import u02.Optionals.Option._

import scala.annotation.tailrec

object Lists {

  // A generic linkedlist
  sealed trait List[E]

  // a companion object (i.e., module) for List
  object List {
    case class Cons[E](head: E, tail: List[E]) extends List[E]
    case class Nil[E]() extends List[E]

    def sum(l: List[Int]): Int = l match {
      case Cons(h, t) => h + sum(t)
      case _ => 0
    }

    def append[A](l1: List[A], l2: List[A]): List[A] = (l1, l2) match {
      case (Cons(h, t), l2) => Cons(h, append(t, l2))
      case _ => l2
    }

    def map[A,B](l: List[A])(mapper: A=>B): List[B] = l match {
      case Cons(h, t) => Cons(mapper(h), map(t)(mapper))
      case Nil() => Nil()
    }

    def mapFlatMap[A,B](l: List[A])(mapper: A=>B): List[B] = flatMap(l)(e=>Cons(mapper(e),Nil()))

    def filter[A](l1: List[A])(pred: A=>Boolean): List[A] = l1 match {
      case Cons(h,t) if pred(h) => Cons(h, filter(t)(pred))
      case Cons(_,t) => filter(t)(pred)
      case Nil() => Nil()
    }

    def filterFlatMap[A](l1: List[A])(pred: A=>Boolean): List[A] = flatMap(l1)(
      e => pred(e) match {
        case true => Cons(e,Nil())
        case _ => Nil()
    })

    @tailrec
    def drop[A](l1: List[A],i:Int): List[A] = l1 match {
      case Cons(_,t) if i>0 => drop(t, i-1)
      case x => x
    }

    def flatMap[A,B](l: List[A])(f: A => List[B]): List[B] = l match {
      case Cons(h,t) => append(f(h),flatMap(t)(f))
      case _ => Nil()

    }

    @tailrec
    def max(l: List[Int]): Option[Int] = l match {
      case Cons(h, Cons (t,t2)) => max(Cons(Math.max(h,t),t2))
      case Cons(h, _) => Some(h)
      case _ => None()
    }

    @tailrec
    def foldLeft[A,B](l: List[A])(initVal: B)(f: (B,A)=>B):B = l match {
      case Cons(h, t) => foldLeft(t)(f(initVal,h))(f)
      case Nil() => initVal
    }


    def foldRight[A,B](l: List[A])(initVal: B)(f: (A,B)=>B):B = l match {
      case Cons(h, t) => f(h,foldRight(t)(initVal)(f))//foldLeft(t)(f(initVal,h))(f)
      case Nil() => initVal
    }

    @tailrec
    private def reverseTailRec[A](l: List[A])(aggregator: List[A]):List[A] = l match {
      case Cons(h, t) => reverseTailRec(t)(append(Cons(h,Nil()),aggregator))
      case Nil() => aggregator
    }

    def reverse[A](l: List[A]):List[A] = reverseTailRec(l)(Nil())

    def foldRightUsingTailRec[A,B](l: List[A])(initVal: B)(f: (A,B)=>B):B = foldLeft(reverse(l))(initVal)((b,a) => f(a,b))

  }
}

object ListsMain extends App {
  import Lists._
  val l = List.Cons(10, List.Cons(20, List.Cons(30, List.Nil())))
  println(List.sum(l)) // 60
  import List._
  println(append(Cons(5, Nil()), l)) // 5,10,20,30
  println(filter[Int](l)(_ >=20)) // 20,30
}