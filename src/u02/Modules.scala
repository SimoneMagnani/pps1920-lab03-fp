package u02

import u02.SumTypes.{Person, Teacher}
import u03.Lists
import u03.Lists.List

object Modules extends App {

  // An ADT: type + module
  sealed trait Person
  object Person {

    case class Student(name: String, year: Int) extends Person

    case class Teacher(name: String, course: String) extends Person

    def name(p: Person): String = p match {
      case Student(n, _) => n
      case Teacher(n, _) => n
    }

    def getCourses(l: Lists.List[Person]): List[String] = List.mapFlatMap(
      List.filterFlatMap(l)(p => p match {
        case Teacher(_,_) => true
        case _ => false}))(
      t=>t match {
        case Teacher(_,c) => c
        case _ => ""})
    
    def getCoursesFlatMap(l: Lists.List[Person]): List[String] = List.flatMap(l)(p => p match {
      case Teacher(_,c) => List.Cons(c,List.Nil());
      case _ => List.Nil()
    })
  }

  println(Person.name(Person.Student("mario",2015)))

  import Person._
  println(name(Student("mario",2015)))

}
